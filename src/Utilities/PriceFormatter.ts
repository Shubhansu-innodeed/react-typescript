const CURRENCY_FORMATTER= new Intl.NumberFormat(undefined,{
    currency: 'USD', style:'currency'
})

export function PriceFormatter(Number:number){
    return CURRENCY_FORMATTER.format(Number)
}