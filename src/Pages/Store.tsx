import React, { useEffect, useState } from 'react'
import {StoreItem} from '../Component/StoreItem';
import {StoreItemProps} from '../Model/Model';


export default function Store() {
  const [products, setProducts] = useState([])
  useEffect(() => {
    fetch('https://fakestoreapi.com/products')
      .then(res => res.json())
      .then(json => setProducts(json))
  }, [])  
  
  return (
    <div className='d-flex align-content-center flex-wrap ' style = {{paddingTop:"50px"}}>{products.map((item: StoreItemProps) =>
      <StoreItem key={item.id} {...item} />
      //id={item.id} title={item.title} price={item.price} image={item.image} description={item.description}
    )}
    </div>
  )
}
