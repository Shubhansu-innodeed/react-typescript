import React from 'react';
import { Route, Routes } from 'react-router';
import Navbar from './Component/Navbar';
import About from './Pages/About';
import Home from './Pages/Home';
// import {ItemPage} from './Pages/ItemPage';
import Store from './Pages/Store';

function App() {
  
  return (
    <>
    <Navbar/>
    <div style={{ backgroundColor:'#faf8f8' }}>
      <Routes>
        <Route path='/' element={<Home/>} />
        <Route path='/about' element={<About/>} />
        <Route path='/store' element={<Store/>} />
        {/* <Route path='/store/item' element={<ItemPage/>} /> */}
      </Routes>
    </div>
    </>
  );
}

export default App;
