import Card from 'react-bootstrap/Card';
import { StoreItemProps } from '../Model/Model';
import StarRate from '@mui/icons-material/StarRateTwoTone';
import { PriceFormatter } from '../Utilities/PriceFormatter';
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Decreasce_Total_Product, Increasce_Total_Product, } from '../Store/Action/Index';

export function StoreItem(props: StoreItemProps) {
    const totalProducts = useSelector((state: any) => state.totalProducts)
    const dispatch = useDispatch();
    const addItems =()=>{
        dispatch(Increasce_Total_Product(1))
    }
    const removeItems =()=>{
        dispatch(Decreasce_Total_Product(1))
    }

    return (
        <div>
            <Card style={{ width: '20rem', height: '30rem', margin: '10px' }} >
                <Card.Img variant="top" src={props.image} height='150px' style={{ objectFit: 'contain', paddingTop:'10px' }} />
                <Card.Body>
                    <Card.Body className='d-flex justify-content-around align-items-baseline h-25'>
                        <Card.Subtitle className="mb-2 text-muted"><StarRate />{props.rating.rate}</Card.Subtitle>
                        <Card.Subtitle className="mb-2 text-muted">{props.rating.count}</Card.Subtitle>
                    </Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <Card.Body>
                        <Card.Text >{PriceFormatter(props.price)}</Card.Text>
                        <div>
                            {totalProducts === 0 ? (<Button 
                            onClick={() => addItems()}
                            className='w-100' >Add to Cart</Button>) : (
                                <div
                                    className="d-flex align-items-center flex-column"
                                    style={{ gap: ".5rem" }}
                                >
                                    <div
                                        className="d-flex align-items-center justify-content-center"
                                        style={{ gap: ".5rem" }}
                                    >
                                        <Button
                                            onClick={() => removeItems()}
                                        >-</Button>
                                        <div>
                                            <span className="fs-3">
                                            {/* {totalProducts} */}
                                                </span> in cart
                                        </div>
                                        <Button
                                            onClick={() => addItems()}
                                        >+</Button>
                                    </div>
                                    <Button
                                        // onClick={() => removeFromCart(id)}
                                        variant="danger"
                                        size="sm"
                                    >
                                        Remove
                                    </Button>
                                </div>
                            )}
                        </div>
                    </Card.Body>
                </Card.Body>
            </Card>
        </div>
    )
}
