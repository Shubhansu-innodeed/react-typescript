import { ADD_TO_CART, DecreasceTotalProduct, IncreasceTotalProduct, REMOVE_FROM_CART } from '../Action/ActionTypes';


export const CartItems = (state: any = [], action: any) => {
    switch (action.type) {
        case ADD_TO_CART:
            return {
                ...state,
                state: state + action.payload
            }
        case REMOVE_FROM_CART:
            return {
                ...state,
                state: state + action.payload
            }
        default:
            return state
    }
}

export const totalProducts = (state: number = 0, action: any) => {
    switch (action.type) {
        case IncreasceTotalProduct:
            // console.log(action);
            return  state + action.payload;
        case DecreasceTotalProduct:
            return state - action.payload;
        default:
            return state
    }
}
