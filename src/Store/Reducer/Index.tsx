import { combineReducers } from "redux";
import { CartItems, totalProducts } from "./CounterReducer";

 const reducers = combineReducers(
    {
        CartItems,totalProducts
    }
)
export default reducers