import { ADD_TO_CART,REMOVE_FROM_CART,DELETE_FROM_CART,IncreasceTotalProduct,DecreasceTotalProduct } from "./ActionTypes";
import { StoreItemProps } from '../../Model/Model';


export const addToCart = (item : StoreItemProps)=>{
    return{
        type: ADD_TO_CART,
        payload: item
    }
}

export const removeFromCart = (item : StoreItemProps)=>{
    return{
        type: REMOVE_FROM_CART,
        payload: item
    }
}

export const deleteFromCart = (item : StoreItemProps)=>{
    return{
        type: DELETE_FROM_CART,
        payload: item
    }
}
export const Increasce_Total_Product = (totalProducts : number)=>{
    return{
        type: 'IncreasceTotalProduct',
        payload: totalProducts
    }
}
export const Decreasce_Total_Product = (totalProducts : number)=>{
    return{
        type: 'DecreasceTotalProduct',
        payload: totalProducts
    }
}