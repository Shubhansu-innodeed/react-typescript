import {createStore} from 'redux'
import reducers from './Reducer'

export const Store = createStore(reducers,{})